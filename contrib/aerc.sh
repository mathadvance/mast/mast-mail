out=$(mast-mail $1)
status=$?

if [[ $1 = '-h' || $1 = '--help' ]]; then
	exit 0
fi

if [[ $1 = 'staff' ]]; then
	emails="aerc \"mailto:"
else
	emails="aerc \"mailto:?bcc="
fi

if [ $status -eq 0 ]; then
	printf '%b\n' $out | {
		while IFS= read -r string; do
			emails=$emails$string,
		done
		emails=$emails\"
		emails=$(echo $emails | sed "s/+/%2B/g")
		eval $emails
	}
fi
