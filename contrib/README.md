# Contributor Scripts

If you have a mast-mail wrapper that you use in your workflow, feel free to send a pull request.

## aerc.sh

Your version of aerc must be sufficiently high. (Notably, the version of aerc in the Arch Linux repositories, which is 0.7.1-1, is not high enough.) If you are using aerc, I recommend you build from source.
