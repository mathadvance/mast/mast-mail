mod api;
mod config;

use strum_macros::EnumString;

use clap::Parser;

#[derive(Parser)]
struct Cli {
    /// Allowed recipient groups: staff, students, applicants, users
    recipient: Group,
}

#[derive(Debug, EnumString)]
#[strum(serialize_all = "snake_case")]
pub enum Group {
    Staff,
    Students,
    Applicants,
    Users,
}

fn main() {
    let config = crate::config::parse_config();
    let args = Cli::parse();
    let emails = crate::api::get_emails(&config.url, &config.pat, &args.recipient);
    for email in emails {
        println!("{email}");
    }
}
