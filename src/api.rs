use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
struct User {
    email: String,
    power: i32,
    #[serde(rename = "Settings")]
    settings: Settings,
}

#[derive(Debug, Serialize, Deserialize)]
struct Settings {
    emails: Option<String>
}

pub fn get_emails(url: &str, pat: &str, group: &crate::Group) -> Vec<String> {
    let mut body: HashMap<&str, &str> = HashMap::new();
    body.insert("token", pat);
    body.insert("tokenType", "PAT");
    let client = reqwest::blocking::Client::new();

    let users = client
        .get(String::from(url) + "/api/staff/users")
        .json(&body)
        .send()
        .unwrap()
        .json::<Vec<User>>()
        .unwrap();

    let iter = users.iter().filter(|&user| match group {
        crate::Group::Staff => user.power >= 4,
        crate::Group::Students => user.power >= 3,
        crate::Group::Applicants => user.power == 2,
        crate::Group::Users => true,
    });

    iter.filter(|user| user.settings.emails != Some(String::from("no"))).map(|user| String::from(&user.email)).collect()
}
