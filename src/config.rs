use dirs::config_dir;
use serde::{Deserialize, Serialize};
use std::{fs, io};

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub url: String,
    pub pat: String,
}

fn initialize() -> Config {
    fn prompt(message: &str) -> String {
        println!("{}", message);
        let mut answer = String::new();
        io::stdin()
            .read_line(&mut answer)
            .expect("Could not read input line");
        String::from(answer.trim())
    }

    println!("Welcome to mast-mail!");
    println!("It seems that this is your first time using the program.");
    println!("To use the program, we'll need some information.");

    let config = Config {
        url: prompt("URL of your mast-web instance (if you don't understand this question, input `https://mast.mathadvance.org`)"),
        pat: prompt("PAT for your mast-web account"),
    };

    if !config_dir().unwrap().join("mast-mail").exists() {
        fs::create_dir(config_dir().unwrap().join("mast-mail"))
            .expect("Could not create mast-mail configuration directory");
    }
    fs::write(
        config_dir().unwrap().join("mast-mail").join("config"),
        toml::to_string(&config).expect("Could not convert Config object into string"),
    )
    .expect("Could not write to mast-mail configuration file");

    config
}

pub fn parse_config() -> Config {
    match fs::read_to_string(config_dir().unwrap().join("mast-mail").join("config")) {
        Ok(string) => {
            toml::from_str(&string).expect("Could not parse mast-mail configuration file")
        }
        Err(_) => initialize(),
    }
}
