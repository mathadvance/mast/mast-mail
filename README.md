# MAST mass-mailing script

The MAST mass-mailing script is meant to assist mass-mailing, typically to the students or staff list, of the MAST program. It works closely in tandem with the [official MAST website](mast.mathadvance.org)'s API ([source](https://gitlab.com/mathadvance/mast/mast-web)).

## Install

The script is written in Rust and compiled with Nightly, so you will need to have Rust installed and you need to switch to the nightly toolchain via
rustup default nightly

Then, run

    git clone https://gitlab.com/mathadvance/mast/mast-mail.git
    cargo install --path .

Now you can use the `mast-mail` binary! (As long as `~/.cargo/bin` is in your `PATH`.)

## Usage

The script will output a list of emails separated by newlines. It is my strong recommendation that you create a wrapper script around `mast-mail` which opens your preferred email client. I personally use `aerc`, and you can find my Linux script in `contrib/aerc.sh`.

To start, you will need to configure `secrets.toml`. There is an `example.secrets.toml` example that you can copy; modify it appropriately.

You will need to be a staff member or higher in your selected instance of the MAST website.

You can email any of the following groups:

- All staff
- All students/staff
- All applicants
- All users (DO NOT USE UNLESS ABSOLUTELY NECESSARY!)

The appropriate commands for each are

- `mast-mail staff`
- `mast-mail students`
- `mast-mail applicants`
- `mast-mail users`

## Configuration

The mast-mail script respects your EDITOR environment variable. If it is not found, it will default to Nano (Mac/Linux) or Notepad (Windows).
